import './App.css';
import LoginForm from './component/LoginForm';
import LogOutForm from './component/LogOutForm';
import { useSelector } from 'react-redux';

function App() {
  const isLoggedIn = useSelector((state) => state.auth.isAuthentificated);
  return (
    <div className="App">
      <>
        {isLoggedIn ? (
          <>
            <LogOutForm />
          </>
        ) : (
          <LoginForm />
        )}
      </>
    </div>
  );
}

export default App;
