import css from './LogOutForm.module.css';
import { useDispatch, useSelector } from 'react-redux';
import { authActions } from '../store/AuthRedux';

const LogOutForm = () => {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state) => state.auth.isAuthentificated);
  const logoutHandler = () => {
    dispatch(authActions.logout());
  };
  return (
    <div className={css['welcome-section']}>
      <h2>You are connected!</h2>
      {isLoggedIn ? (
        <button className={css.btn} onClick={logoutHandler}>
          Sign Out
        </button>
      ) : (
        <button>Login</button>
      )}
    </div>
  );
};
export default LogOutForm;
