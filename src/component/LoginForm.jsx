import css from './LoginForm.module.css';
import { useDispatch } from 'react-redux';
import { authActions } from '../store/AuthRedux';
import { useState, useEffect, React } from 'react';

const LoginForm = () => {
  const [email, setEmail] = useState('', () => {
    const localData = localStorage.getItem('email');
    return localData ? JSON.stringify(localData) : [];
  });
  const [password, setPassword] = useState('');

  useEffect(() => {
    localStorage.setItem('email', 'password', JSON.stringify(email, password));
  }, [email, password]);

  const emailHandler = (e) => {
    setEmail(e.target.value);
  };
  const passwordHandler = (e) => {
    setPassword(e.target.value);
  };

  const dispatch = useDispatch();
  const loginHandler = (e) => {
    e.preventDefault();
    console.log('login was successfuly');

    const user = {
      email: email,
      password: password,
    };
    dispatch(authActions.login(user));
  };
  return (
    <div className={css['main-container']}>
      <div className={css.header}>
        <h5>Login Form</h5>
      </div>
      <form onSubmit={loginHandler} className={css['input-block']}>
        <div className={css['form-group']}>
          <input
            onChange={emailHandler}
            value={email}
            type="email"
            className={css['form-control']}
            placeholder="Username"
            id="email"
            required
          />
          <span className={css.span}>
            <i class="fa fa-user" aria-hidden="true"></i>
          </span>
        </div>
        <div className={css['form-group']}>
          <input
            onChange={passwordHandler}
            value={password}
            type="password"
            className={css['form-control']}
            placeholder="Password"
            id="password"
            required
          />
          <span className={css.span}>
            <i class="fa fa-lock" aria-hidden="true"></i>
          </span>
        </div>
        <div className={css['btn-container']}>
          <button className={css['signIn-btn']}>Sign In</button>
        </div>
      </form>
    </div>
  );
};

export default LoginForm;
