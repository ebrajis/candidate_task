import { createSlice } from '@reduxjs/toolkit';

const authUser = {
  isAuthentificated: false,
  user: {
    email: 'frontend@isawesome.com',
    password: 'cool',
  },
};

const authSlice = createSlice({
  name: 'authentication',
  initialState: authUser,
  reducers: {
    login(state, action) {
      if (authUser.user.email === action.payload.email && authUser.user.password === action.payload.password) {
        state.isAuthentificated = true;
      }
    },
    logout(state) {
      state.isAuthentificated = false;
    },
  },
});
export const authActions = authSlice.actions;
export default authSlice.reducer;
